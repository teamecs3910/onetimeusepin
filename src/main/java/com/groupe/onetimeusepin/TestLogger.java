package com.groupe.onetimeusepin;


import org.aopalliance.intercept.Joinpoint;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.*;

@Aspect
@Component
public class TestLogger {
    private Logger logger = Logger.getLogger(getClass());


    @AfterReturning(pointcut = "execution(* com.groupe.onetimeusepin.PinController.*(..))", returning = "value")
    public void TestLog(JoinPoint point, Object value){
        logger.info("Ran " + point.getSignature().getName() + " on ip " +
                ((HttpServletRequest) point.getArgs()[point.getArgs().length - 1]).getRemoteAddr());
        logger.info(value);
    }

}
