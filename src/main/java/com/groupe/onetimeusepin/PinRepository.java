package com.groupe.onetimeusepin;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PinRepository extends CrudRepository<Pin, Long> {
    List<Pin> findByAccountAndClaimIPIsNull(String account);
    List<Pin> findByPin(String pinNumber);
    Pin findOneByPin(String pinNumber);
    Pin findOneByAccountAndClaimIPIsNull (String account);
}