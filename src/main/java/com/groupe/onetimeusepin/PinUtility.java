package com.groupe.onetimeusepin;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;

class PinUtility {
    private static final int MILLISECONDS_IN_MINUTE = 60000;

    static String getObjectAsJSON(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        String result  = "";
        try {
            result = mapper.writeValueAsString(object);
        } catch(IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    static Date getExpireTime(int lifetime) {
        Date date = new Date();
        long time = date.getTime() + lifetime * MILLISECONDS_IN_MINUTE;
        return new Date(time);
    }
}