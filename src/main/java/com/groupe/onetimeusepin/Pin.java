package com.groupe.onetimeusepin;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "group_e_pin")
public class Pin {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 256)
    private String account;

    @NotNull
    @Size(max = 6)
    @Column(unique = true)
    private String pin;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTimestamp;

    @NotNull
    @Size(max = 256)
    private String createIP;

    @NotNull
    @Size(max = 256)
    private String createUser;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date expireTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    private Date claimTimestamp;

    @Size(max = 256)
    private String claimUser;

    @Size(max = 256)
    private String claimIP;

    public Pin() {}

    public Pin(String account, String pin, Date createTimestamp, String createIP, String createUser, Date expireTimestamp) {
        this.account = account;
        this.pin = pin;
        this.createTimestamp = createTimestamp;
        this.createIP = createIP;
        this.createUser = createUser;
        this.expireTimestamp = expireTimestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public String getCreateIP() {
        return createIP;
    }

    public void setCreateIP(String createIP) {
        this.createIP = createIP;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getExpireTimestamp() {
        return expireTimestamp;
    }

    public void setExpireTimestamp(Date expireTimestamp) {
        this.expireTimestamp = expireTimestamp;
    }

    public Date getClaimTimestamp() {
        return claimTimestamp;
    }

    public void setClaimTimestamp(Date claimTimestamp) {
        this.claimTimestamp = claimTimestamp;
    }

    public String getClaimUser() {
        return claimUser;
    }

    public void setClaimUser(String claimUser) {
        this.claimUser = claimUser;
    }

    public String getClaimIP() {
        return claimIP;
    }

    public void setClaimIP(String claimIP) {
        this.claimIP = claimIP;
    }
}