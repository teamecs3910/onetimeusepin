package com.groupe.onetimeusepin;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/otup")
public class PinController {
    @Autowired private PinRepository pinRepository;
    @Value("${pinLifetime}") private int pinLifetime;
    private final int PIN_NUMBER_SIZE = 6;

    @RequestMapping(path = "/request", method = RequestMethod.POST, produces = "application/json")
    public String requestPIN(
            @RequestParam("request-account") String account,
            @RequestParam("request-user") String createUser,
            HttpServletRequest request) {
        String message;
        String jsonPin = "";
        if (!account.matches("[0-9]{1,256}")) {
            message = "Account number '" + account + "' is invalid.";
        } else if(!createUser.matches("[a-zA-Z]{1,256}")) {
            message = "User name '" + createUser + "' is invalid.";
        } else if (this.pinRepository.findByAccountAndClaimIPIsNull(account).isEmpty()) {
            String pinNumber = M10.getUniquePINNumber(this.pinRepository, PIN_NUMBER_SIZE);
            Pin pin = new Pin(
                    account,
                    pinNumber,
                    new Date(),
                    request.getRemoteAddr(),
                    createUser,
                    PinUtility.getExpireTime(this.pinLifetime));
            try {
                this.pinRepository.save(pin);
            } catch (DataAccessException e) {
                e.printStackTrace();
            }
            message = "PIN created successfully.";
            jsonPin = "\"pin\":" + PinUtility.getObjectAsJSON(pin);
        } else {
            message = "PIN is already active for account '" + account + "'.";
        }
        return "{\"message\":\"" + message + "\"" + (jsonPin.isEmpty() ? "" : "," + jsonPin )+ "}";
    }

    @RequestMapping(path = "/claim", method = RequestMethod.POST, produces = "application/json")
    public String claimPIN(
            @RequestParam("claim-pin") String pinNumber,
            @RequestParam("claim-user") String claimUser,
            HttpServletRequest request) {
        String message;
        String jsonPin = "";
        if (!claimUser.matches("[a-zA-Z]{1,256}")) {
            message = "Claim user '" + claimUser + "' is invalid.";
        } else if (!pinNumber.matches("[1-9][0-9]{" + (PIN_NUMBER_SIZE - 1) + "}") || !M10.isLuhnValid(pinNumber)) {
            message = "Pin number '" + pinNumber + "' is invalid.";
        } else {
            Pin pin = this.pinRepository.findOneByPin(pinNumber);
            if (pin == null) {
                message = "PIN number '" + pinNumber + "' not found.";
            } else if (pin.getClaimTimestamp() != null) {
                message = "PIN number '" + pinNumber + "' has already been claimed.";
            } else if(pin.getExpireTimestamp().before(new Date())) {
                return expirePin(pinNumber, request);
            } else {
                pin.setClaimIP(request.getRemoteAddr());
                pin.setClaimTimestamp(new Date());
                pin.setClaimUser(claimUser);
                try {
                    this.pinRepository.save(pin);
                } catch (DataAccessException e) {
                    e.printStackTrace();
                }
                message = "PIN number " + pinNumber + " claimed.";
                jsonPin = "\"pin\":" + PinUtility.getObjectAsJSON(pin);
            }
        }
        return "{\"message\":\"" + message + "\"" + (jsonPin.isEmpty() ? "" : "," + jsonPin )+ "}";
    }

    @RequestMapping(path = "/cancel", method = RequestMethod.POST, produces = "application/json")
    public String cancelPIN(@RequestParam("cancel-account") String cancelAccount, HttpServletRequest request) {
        String message;
        String jsonPin = "";
        if (!cancelAccount.matches("[0-9]{1,256}")) {
            message = "Account number '" + cancelAccount + "' is invalid.";
        } else {
            Pin pin = this.pinRepository.findOneByAccountAndClaimIPIsNull(cancelAccount);
            if (pin == null) {
                message = "Invalid Account Number";
            } else if (pin.getClaimIP() != null) {
                message = "PIN has already been canceled.";
            } else {
                pin.setClaimIP(request.getRemoteAddr());
                pin.setClaimTimestamp(new Date());
                pin.setClaimUser(pin.getCreateUser());
                try {
                    this.pinRepository.save(pin);
                } catch (DataAccessException e) {
                    e.printStackTrace();
                }
                message = "Canceled PIN number " + pin.getPin() + ".";
                jsonPin = "\"pin\":" + PinUtility.getObjectAsJSON(pin);
            }
        }
        return "{\"message\":\"" + message + "\"" + (jsonPin.isEmpty() ? "" : "," + jsonPin )+ "}";
    }

    @RequestMapping(path = "/expire", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String expirePin(@RequestParam("expire-pin") String pinNumber, HttpServletRequest request) {
        String message;
        String jsonPin = "";
        if(!pinNumber.matches("[1-9][0-9]{" + (PIN_NUMBER_SIZE - 1) + "}")) {
            message = "PIN number '" + pinNumber + "' is invalid.";
        } else {
            Pin pin = this.pinRepository.findOneByPin(pinNumber);
            if (pin == null) {
                message = "PIN number " + pinNumber + " not found.";
            } else if (pin.getClaimIP() != null) {
                message = "PIN '" + pinNumber + "' has already been claimed.";
            } else if (pin.getExpireTimestamp().after(new Date())) {
                message = "PIN '" + pinNumber + "' is not ready to expire.";
            } else {
                pin.setClaimIP(request.getRemoteAddr());
                pin.setClaimTimestamp(new Date());
                pin.setClaimUser("System");
                try {
                    this.pinRepository.save(pin);
                } catch (DataAccessException e) {
                    e.printStackTrace();
                }
                message = "Expired PIN number '" + pinNumber + "'.";
                jsonPin = "\"pin\":" + PinUtility.getObjectAsJSON(pin);
            }
        }
        return "{\"message\":\"" + message + "\"" + (jsonPin.isEmpty() ? "" : "," + jsonPin )+ "}";
    }
}