package com.groupe.onetimeusepin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneTimeUsePinApplication {
	public static void main(String[] args) {
		SpringApplication.run(OneTimeUsePinApplication.class, args);
	}
}
