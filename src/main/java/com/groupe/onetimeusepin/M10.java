package com.groupe.onetimeusepin;

import java.util.Random;

public class M10 {
    private static Random random = new Random();

    public static String getUniquePINNumber(PinRepository repository, int pinNumberSize) {
        String pinNumber = generatePinNumber(pinNumberSize);
        while (!repository.findByPin(pinNumber).isEmpty()) {
            pinNumber = generatePinNumber(pinNumberSize);
        }
        return pinNumber;
    }

    public static boolean isLuhnValid(String pinNumber) {
        int[] pinDigits = getIntArrayFromString(pinNumber, 0);
        return (getLuhnWeightedTotal(pinDigits) % 10 == 0);
    }

    private static String addCheckDigit(String pinNumber) {
        int[] pinDigits = getIntArrayFromString(pinNumber, 1);
        pinDigits[pinDigits.length - 1] = (getLuhnWeightedTotal(pinDigits) * 9) % 10;
        return getStringFromIntArray(pinDigits);
    }

    private static String generatePinNumber(int pinNumberSize) {
        if(pinNumberSize < 2) {
            return "0";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((random.nextInt(9) + 1)); //generate random number 1-9
        for(int i = 0; i < pinNumberSize - 2; i++) {
            stringBuilder.append(random.nextInt(9)); //generate random number 0-9
        }
        return addCheckDigit(stringBuilder.toString());
    }

    private static int getLuhnWeightedTotal(int[] pinDigits) {
        int total = 0;
        for(int i = 0; i < pinDigits.length; i++) {
            int convertedValue = pinDigits[i];
            if(i % 2 == 0) {
                convertedValue *= 2;
                if(convertedValue > 9) {
                    convertedValue -= 9;
                }
            }
            total += convertedValue;
        }
        return total;
    }

    private static int[] getIntArrayFromString(String input, int expandArraySize) {
        String[] inputArray = input.split("");
        int[] output = new int[inputArray.length + expandArraySize];
        for(int i = 0; i < output.length; i++) {
            if(i < inputArray.length) {
                try {
                    output[i] = Integer.parseInt(inputArray[i]);
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
            } else {
                output[i] = 0;
            }
        }
        return output;
    }

    private static String getStringFromIntArray(int[] input) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int digit : input) {
            stringBuilder.append(digit);
        }
        return stringBuilder.toString();
    }
}