function processForm(form) {
    event.preventDefault();
    $('#message-status').text("");
    $('#pin-status').text("");
    var formData = new FormData();
    var targetUrl;
    var selectedAction = document.activeElement.name.split("-")[0];
    switch(selectedAction){
        case 'request':
            formData.append("request-account", form.elements['request-account'].value);
            formData.append("request-user", form.elements['request-user'].value);
            targetUrl = './otup/request';
            break;
        case 'claim':
            formData.append("claim-pin", form.elements['claim-pin'].value);
            formData.append("claim-user", form.elements['claim-user'].value);
            targetUrl = './otup/claim';
            break;
        case 'cancel':
            formData.append("cancel-account", form.elements['cancel-account'].value);
            targetUrl = './otup/cancel';
            break;
        case 'expire':
            formData.append("expire-pin", form.elements['expire-pin'].value);
            targetUrl = './otup/expire';
            break;
        default:
            alert('Form submission error');
            return;
    }
    performAjax(formData, targetUrl);
}

function performAjax(formData, url) {
    var ajaxOptions = new Object();
    ajaxOptions.data = formData;
    ajaxOptions.type = 'POST';
    ajaxOptions.url = url;
    ajaxOptions.dataType = "json";
    ajaxOptions.processData = false;
    ajaxOptions.contentType = false;
    ajaxOptions.success = function(response) {
        $('#message-status').text(response.message);
        $('#pin-status').text(JSON.stringify(response.pin));
    };
    $.ajax(ajaxOptions);
}